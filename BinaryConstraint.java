import java.util.LinkedList;

public class BinaryConstraint {


    public String variableOne;
    public String variableTwo;
    public String type = "BinaryConstraint";

    //constraint is either either equal values or unequal valus
    public boolean equalValues;
    
    BinaryConstraint(String variableOne, String variableTwo, boolean equalValues) {
        this.variableOne = variableOne;
        this.variableTwo = variableTwo;
        this.equalValues = equalValues;
    }


    public boolean satisfied(String valueOfVariableOne, String valueOfVariableTwo) {

        if(equalValues) {
            if(valueOfVariableOne.equals(valueOfVariableTwo)) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if(!valueOfVariableOne.equals(valueOfVariableTwo)) {
                return true;
            }
            else {
                return false;
            }
        }
    
    }

    public LinkedList<String> getConstrainedVariable() {
        LinkedList<String> tmp = new LinkedList<>();
        tmp.add(variableOne);
        tmp.add(variableTwo);
        return tmp;
    }

    public String getType() {
        return this.type;
    }
    
}
