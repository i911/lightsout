import java.util.LinkedList;


public class arcConsistency {

    public boolean check(Csp csp) {

        LinkedList<BinaryConstraint> constraints = csp.constraints;
        LinkedList<UnaryConstraint> unaryConstraints = csp.unaryConstraints;

        unaryConstraints.forEach(
            k -> {
                this.reviseUnary(csp, k);
            }
        );

        for(int k = 0; k<constraints.size(); k++) {
            BinaryConstraint currentConstraint = constraints.get(k);

            if(revise(csp, currentConstraint)) {
                
             

        }
    }


        return true;
    }

    //TODO: if domain size = 1  -> assign value

    public boolean revise(Csp csp, BinaryConstraint constraint) {

        boolean revised = false;

        int satisfiedValues = 0;
        String satisfiedValue = " ";

        LinkedList<String> domainVariableTwo = csp.domains.get(constraint.variableOne);
        LinkedList<String> domainVariableOne = csp.domains.get(constraint.variableTwo);

        if(!constraint.equalValues && csp.variables.containsKey(constraint.variableOne)) {
            if(csp.variables.get(constraint.variableOne).equals("*") && csp.variables.get(constraint.variableTwo).equals(" ")) {
                csp.assignValue(constraint.variableTwo, "+");
            }
        }
        
        return revised;
    }

    public void reviseUnary(Csp csp, UnaryConstraint constraint) {
        if(constraint.ForbiddenValue.equals("*") && csp.variables.get(constraint.variable).equals(" ")) {
            csp.assignValue(constraint.variable, "+");
        }
        if(constraint.ForbiddenValue.equals("+") && csp.variables.get(constraint.variable).equals(" ")) {
            csp.assignValue(constraint.variable, "*");
        }
    }
}
