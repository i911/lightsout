import java.util.LinkedList;
import java.util.Map;

public class backTracking {

    int backtrackingSteps = 0;
    int iterations = 0;
    Csp lastResult;
    domainConstraint domainConstraint = new domainConstraint();
    
    public Csp chronologicalBacktracking(Csp csp) {

            //csp.displayField();
            int variableCount = 0;
            for(Map.Entry<String, String> entry : csp.variables.entrySet()) {
                if(entry.getValue().equals("*") || entry.getValue().equals("+")) {
                variableCount++;
                }
                else if(entry.getValue().equals(" ")) {
                    break;
                }
            
                if(variableCount == csp.variables.size()) {
                 return csp;
                }
            }
       
        
        Csp result = new Csp(csp);

        //get next variable based on heuristics
        String variable = result.heuristic.determineNextVariable();
        //System.out.println(variable);
        String value = "";

        //determine order in which we iterate values of the domain
        LinkedList<String> domainOrder = this.domainOrder(variable, result);

        //iterate domain
        for(int i = 0; i<domainOrder.size(); i++) {

            //System.out.println(variable);

            //current value
            value = domainOrder.get(i);
            //System.out.println(value);

            //remove and assign value
            //csp.removeValue(variable, value);
            result.assignValue(variable, value);

            if(variable.equals("5,5")) {
                System.out.println("");
            }

            
            
            if(result.rangeConstraint.satisfied(result) && result.hintConstraint.satisfied(result) && result.rangeConstraint.lightHasSource(result)) {
                
                if(result.arcConsistency.check(result)) {
                  
                    iterations++;


                    Csp nextIteration = chronologicalBacktracking(result);

                    if(nextIteration != null) {
                        result = nextIteration;
                       
                        return result;
                        
                    }
                }

                
            }
        }

    
        backtrackingSteps++;
        lastResult = csp;
        return null;
     
    }

    //"+" is preferred for value selection, because it is generally less constraint than "*"
    public LinkedList<String> domainOrder(String variable, Csp csp) {
        LinkedList<String> result = new LinkedList<>();

        LinkedList<String> domainOfVariable = csp.domains.get(variable);
        if(domainOfVariable.contains("+")) {
            result.add("+");
        }
        if(domainOfVariable.contains("*")){
            result.add("*");
        }

        return result;
    }
}
