import java.util.LinkedList;

//Constraints:
// - unary:
//   blocks with value 0
// - binary:
//   light bulb field != neighbor
//   blocks with value 1 or more-> if light is planted the other variables get constrained
//   block with value 3
// - higher order:
//   light planted -> all variables left/right/up/down until block get constrained
//   blocks with value 1 or more -> if light is planted the other variable get constrained

public interface Constraint {

    public String variableOne = "";
    public String variableTwo = "";
    public String value = "";
    public String type = "";


    public boolean satisfied(Csp csp);

    public String getType();

    public LinkedList<String> getConstrainedVariable();
}

