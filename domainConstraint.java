import java.util.LinkedList;
import java.util.Map;

public class domainConstraint {
    
    public boolean satisfied(Csp csp) {
        
        for(Map.Entry<String, String> entry : csp.variables.entrySet()) {

            String value = entry.getValue();

            if(value.equals(" ")) {
                return false;
            }
        }

        System.out.println("Domain satisfied");
        return true;
    }
}
