import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Queue;

import javax.management.Query;

            /*define variables and constraints
                    preprocessing % e.g. arc-consistency
                    WHILE there are uninstantiated variables
                        select variable v % selection
                            IF domain of v is empty
                            THEN solve conflict % (backtrack)
                            ELSE select consistent value from % selection
                                domain of v % assignment
                                constraint propagation % propagation
                            ENDIF
                    END WHILE*/

public class Csp {

    int size = 14;

    //TODO: implement unassigned

        //initialzie values
        //light = "+"
        //light bulb = "*"

    public boolean finished = false;

    //contains the set values
    public HashMap<String, String> variables = new HashMap<>();
    //variables which were assigned in the last iteration
    public HashMap<String, String> recentlyAssignedVariables = new HashMap<>();
    //variables that are constrained by unaryConstraint
    public HashMap<String, String> unaryConstrainedVariables = new HashMap<>();

    public LinkedList<String> possibleValues = new LinkedList<>(List.of("+", "*"));
    //variableIndex, List of values
    public HashMap<String, LinkedList<String>> domains = new HashMap<>();
    public LinkedList<BinaryConstraint> constraints = new LinkedList<>();
    public LinkedList<UnaryConstraint> unaryConstraints = new LinkedList<>();
    public domainConstraint domainConstraint = new domainConstraint();
    public rangeConstraint rangeConstraint = new rangeConstraint();
    public hintConstraint hintConstraint = new hintConstraint();

    public HashMap<String, String> blocks = new HashMap<>();
    //blocks of which the surrounding were changed in the last iteration
    public HashMap<String, String> recentlyChangedBlocks = new HashMap<>();

    public arcConsistency arcConsistency = new arcConsistency();

    //empty spaces between black boxes
    public LinkedList<LinkedList<String>> RowRanges = new LinkedList<>();
    public LinkedList<LinkedList<String>> ColumnRanges = new LinkedList<>();
    public HashMap<String, String> hintBlocks = new HashMap<>();

    public variableSelection heuristic = new variableSelection(this);

    public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    //clone constructor, used for saving milestones
    public Csp(Csp anotherCsp) {
        this.finished = anotherCsp.finished;

        for(Map.Entry<String, String> entry : anotherCsp.variables.entrySet()) {
            this.variables.put(entry.getKey(), entry.getValue());
        }

        this.possibleValues = anotherCsp.possibleValues;

        for(Map.Entry<String, LinkedList<String>> entry : anotherCsp.domains.entrySet()) {
            this.domains.put(entry.getKey(), entry.getValue());
        }
        
        for(int i = 0; i<anotherCsp.constraints.size(); i++) {
            this.constraints.add(anotherCsp.constraints.get(i));
        }

        for(int i = 0; i<anotherCsp.unaryConstraints.size(); i++) {
            this.unaryConstraints.add(anotherCsp.unaryConstraints.get(i));
        }
        this.domainConstraint = anotherCsp.domainConstraint;
        this.rangeConstraint = anotherCsp.rangeConstraint;
        this.hintConstraint = anotherCsp.hintConstraint;
        this.blocks = anotherCsp.blocks;
        this.arcConsistency = anotherCsp.arcConsistency;
        this.RowRanges = anotherCsp.RowRanges;
        this.ColumnRanges = anotherCsp.ColumnRanges;
        this.hintBlocks = anotherCsp.hintBlocks;
        this.heuristic = new variableSelection(this);

        //clone recently assigned variables
        for(Map.Entry<String, String> entry : anotherCsp.recentlyAssignedVariables.entrySet()) {
            this.recentlyAssignedVariables.put(entry.getKey(), entry.getValue());
        }
        //clone recently changed blocks
        for(Map.Entry<String, String> entry : anotherCsp.recentlyChangedBlocks.entrySet()) {
            this.recentlyChangedBlocks.put(entry.getKey(), entry.getValue());
        }
        

        this.additionalConstraints();

        this.recentlyAssignedVariables.clear();
        this.recentlyChangedBlocks.clear();

    }

    public Csp() {

    }

    public void initialize() {

        String[][] field = csvReader14x14.getField();

        //initialize variable list
        for(int i = 0; i<this.size; i++) {

            for(int j = 0; j<this.size; j++) {

                if(field[i][j].equals(" ")) {
                    variables.put(Integer.toString(i) + "," + Integer.toString(j), " ");
                    
                }
                else {
                    blocks.put(Integer.toString(i) + "," + Integer.toString(j), field[i][j]);
                }
            }
        }

        //initialize domains list
        //when initialized every variable has "+" and "*" as possible values
        
        variables.forEach(
            (k,v) -> domains.put(k, possibleValues)
        );
        
      

        //fill unary constraints
        for(Map.Entry<String, String> entry: this.getVariables().entrySet()) {
            
                String k = entry.getKey();
                String v = entry.getValue();

                int currentFieldRow = this.getIndexAsInteger(k)[0];
                int currentFieldColumn = this.getIndexAsInteger(k)[1];
                

                /*//consider right field
                if(currentFieldColumn < this.size) {
                    //right neighbor index
                    int rightNeighborRowIndex = currentFieldRow;
                    int rightNeighborColumnIndex = currentFieldColumn + 1;
                
                    //if right field is a block with value 0
                    if(this.getBlock(rightNeighborRowIndex, rightNeighborColumnIndex) != null && this.getBlock(rightNeighborRowIndex, rightNeighborColumnIndex).equals("0")) {
                        addUnaryConstraint(k, "*");
                    }

                }

                //consider left field
                if(currentFieldColumn > 0) {

                    //left neighbor index
                    int leftNeighborRowIndex = this.getIndexAsInteger(k)[0];
                    int leftNeighborColumnIndex = this.getIndexAsInteger(k)[1] - 1;

                    //if left field is a block with value 0
                    if(this.getBlock(leftNeighborRowIndex, leftNeighborColumnIndex) != null && this.getBlock(leftNeighborRowIndex, leftNeighborColumnIndex).equals("0")) {
                        addUnaryConstraint(k, "*");
                    }
                }

                //consider top field
                if(currentFieldRow > 0) {

                    //top neighbor index
                    int topNeighborRowIndex = this.getIndexAsInteger(k)[0] - 1;
                    int topNeighborColumnIndex = this.getIndexAsInteger(k)[1];

                    //if top field is a block with value 0
                    if(this.getBlock(topNeighborRowIndex, topNeighborColumnIndex) != null && this.getBlock(topNeighborRowIndex, topNeighborColumnIndex).equals("0")) {
                        addUnaryConstraint(k, "*");
                    }
                }

                //consider bottom field
                if(currentFieldRow < this.size) {

                    //bottom neighbor index
                    int bottomNeighborRowIndex = this.getIndexAsInteger(k)[0] + 1;
                    int bottomNeighborColumnIndex = this.getIndexAsInteger(k)[1];

                    //if left field is a block with value 0
                    if(this.getBlock(bottomNeighborRowIndex, bottomNeighborColumnIndex) != null && this.getBlock(bottomNeighborRowIndex, bottomNeighborColumnIndex).equals("0")) {
                        addUnaryConstraint(k, "*");
                    }
                }*/

            }

            //TODO: fill higher order constraints
        
        

            this.initializeRowRanges();
            this.initializeColumnRanges();
            this.initializeHintBlocks();
            this.additionalConstraints();
    }


    //constraint cases:
    //-hintblock with surrounding variables == hintnumber
    // -> plus after placement of one or more lamps/lights
    //-placement of lamp -> orthogonal light (check for lamps without light)
    //-hint number = 0 -> lights
    public void additionalConstraints() {
        //iterate through blocks and add constraints for surrounding fields
        // -> check for empty variables next to blocks
        //if hint number == 0, place constraint for lights on variables
        for(Map.Entry<String, String> entry : this.blocks.entrySet()) {

            String fieldIndex = entry.getKey();
            String value = entry.getValue();

            LinkedList<String> surroundingVariables = this.getSurroundingFields(fieldIndex);

            if(value.equals("0")) {
                
                surroundingVariables.forEach(
                    k -> {
                        if(this.variables.containsKey(k) && !this.unaryConstrainedVariables.containsKey(k)) {
                            this.addUnaryConstraint(k, "*");
                        }
                    }
                );
            }
            else if(!value.equals("x")) {
                
                LinkedList<String> surroundingValues = this.checkSurroundingFields(fieldIndex);

                LinkedList<String> emptyVariables = new LinkedList<>();

                int emptyCount = 0;
                int lampCount = 0;

                for(int i = 0; i<surroundingValues.size(); i++) {
                    if(surroundingValues.get(i).equals("*")) {
                        lampCount++;
                    }
                    if(surroundingValues.get(i).equals(" ")) {
                        emptyCount++;
                        //getSurroundingFields and checkSurroundingFields have same order (right, left, top, bottom)
                        emptyVariables.add(surroundingVariables.get(i));
                    }
                }

                if(emptyCount == Integer.parseInt(value) && lampCount == 0 || emptyCount+lampCount == Integer.parseInt(value)) {
                    emptyVariables.forEach(
                        k -> {
                            if(!this.unaryConstrainedVariables.containsKey(k)) {
                                this.addUnaryConstraint(k, "+");
                            }
                        }
                    );
                }   
                
            }

        }
        
        //iterate through variables and add constraints for light resulting of lamps
        for(Map.Entry<String, String> entry : this.recentlyAssignedVariables.entrySet()) {

            String fieldIndex = entry.getKey();
            String value = entry.getValue();

            if(value.equals("*")) {

                LinkedList<String> rangeVariables = this.getRangeVariables(fieldIndex);

                for(int i = 0; i<rangeVariables.size(); i++) {
                    this.addBinaryConstraint(fieldIndex, rangeVariables.get(i));
                }
            }
        }
    
    }
    

    public void addUnaryConstraint(String variable, String forbiddenValue) {
        this.unaryConstraints.add(new UnaryConstraint(variable, forbiddenValue));
        this.unaryConstrainedVariables.put(variable, forbiddenValue);
    }

    public void addBinaryConstraint(String constrainingVariable, String constrainedVariable) {
        constraints.add(new BinaryConstraint(constrainingVariable, constrainedVariable, false));
    }

    //returns list of variables which are in the same range
    //used for light shine of a lamp
    public LinkedList<String> getRangeVariables(String variableIndex) {

        LinkedList<String> result = new LinkedList<>();

        for(int i = 0; i<this.RowRanges.size(); i++) {
            if(this.RowRanges.get(i).contains(variableIndex)) {
                
                this.RowRanges.get(i).forEach(
                    k -> {
                        if(!k.equals(variableIndex)) {
                            result.add(k);
                        }
                    }
                );
            }
        }
        for(int i = 0; i<this.ColumnRanges.size(); i++) {
            if(this.ColumnRanges.get(i).contains(variableIndex)) {
                
                this.ColumnRanges.get(i).forEach(
                    k -> {
                        if(!k.equals(variableIndex)) {
                            result.add(k);
                        }
                    }
                );
            }
        }

        return result;
    }


    //takes the list which contains the coordinates and searches the specific key in the variables list
    //this is done, because the normal key is a list identifier (hashcode)
    public String getBlock(int row, int column) {

        for(Map.Entry<String, String> entry : this.blocks.entrySet()) {

                String k = entry.getKey();
                String v = entry.getValue();

                if(this.getIndexAsInteger(k)[0] == row && this.getIndexAsInteger(k)[1] == column) {
                    
                    return v;
                }
                
            }
        return null;
    }

    //TODO: change putlight to addconstraint
    public void putLamp(String variableField) {

        variables.put(variableField, "*");

        LinkedList<String> newDomain = new LinkedList<>();
        newDomain.add("*");
        domains.put(variableField, newDomain);

        int[] var = this.getIndexAsInteger(variableField);

        String[][] field = this.getFieldAs2DArray();

        //left horizontal light
        for(int i = var[1]-1; i>=0; i--)  {

            if(field[var[0]][i].equals(" ") || field[var[0]][i].equals("+")) {
                this.putLight(Integer.toString(var[0]) + "," + Integer.toString(i));
            }
            else {
                break;
            }
        }

        //right horizontal light
        for(int i = var[1]+1; i<this.size; i++)  {

            if(field[var[0]][i].equals(" ") || field[var[0]][i].equals("+")) {
                this.putLight(Integer.toString(var[0]) + "," + Integer.toString(i));
            }
            else {
                break;
            }
        }

        //top vertical light
        for(int i = var[0]-1; i>=0; i--)  {

            if(field[i][var[1]].equals(" ") || field[i][var[1]].equals("+")) {
                this.putLight(Integer.toString(i) + "," + Integer.toString(var[1]));

            }
            else {
                break;
            }
        }

        //bottom vertical light
        for(int i = var[0]+1; i<this.size; i++)  {

            if(field[i][var[1]].equals(" ") || field[i][var[1]].equals("+")) {
                this.putLight(Integer.toString(i) + "," + Integer.toString(var[1]));
            }
            else {
                break;
            }
        }
    }

    public void putLight(String variableField) {
        variables.put(variableField, "+");

        LinkedList<String> newDomain = new LinkedList<>();
        newDomain.add("+");
        domains.put(variableField, newDomain);
    }

    



    public LinkedList<String> checkSurroundingFields(String index) {

        LinkedList<String> result = new LinkedList<>();

        String[][] field = this.getFieldAs2DArray();

        
            
            String k = index;
            

            int currentFieldRow = this.getIndexAsInteger(k)[0];
            int currentFieldColumn = this.getIndexAsInteger(k)[1];

        //consider right field
        if(currentFieldColumn < this.size-1) {
            //right neighbor index
            int rightNeighborRowIndex = currentFieldRow;
            int rightNeighborColumnIndex = currentFieldColumn + 1;

            result.add(field[rightNeighborRowIndex][rightNeighborColumnIndex]);

        }

        //consider left field
        if(currentFieldColumn > 0) {

            //left neighbor index
            int leftNeighborRowIndex = this.getIndexAsInteger(k)[0];
            int leftNeighborColumnIndex = this.getIndexAsInteger(k)[1] - 1;

            result.add(field[leftNeighborRowIndex][leftNeighborColumnIndex]);
        }

        //consider top field
        if(currentFieldRow > 0) {

            //top neighbor index
            int topNeighborRowIndex = this.getIndexAsInteger(k)[0] - 1;
            int topNeighborColumnIndex = this.getIndexAsInteger(k)[1];

            result.add(field[topNeighborRowIndex][topNeighborColumnIndex]);
        }

        //consider bottom field
        if(currentFieldRow < this.size-1) {

            //bottom neighbor index
            int bottomNeighborRowIndex = this.getIndexAsInteger(k)[0] + 1;
            int bottomNeighborColumnIndex = this.getIndexAsInteger(k)[1];

            result.add(field[bottomNeighborRowIndex][bottomNeighborColumnIndex]);
        }
    

    return result;
    }

    //returns index of surrounding fields
    public LinkedList<String> getSurroundingFields(String index) {

        LinkedList<String> result = new LinkedList<>();

        String[][] field = this.getFieldAs2DArray();

        
            
            String k = index;
            

            int currentFieldRow = this.getIndexAsInteger(k)[0];
            int currentFieldColumn = this.getIndexAsInteger(k)[1];

        //consider right field
        if(currentFieldColumn < this.size-1) {
            //right neighbor index
            int rightNeighborRowIndex = currentFieldRow;
            int rightNeighborColumnIndex = currentFieldColumn + 1;

            result.add(Integer.toString(rightNeighborRowIndex) + "," + Integer.toString(rightNeighborColumnIndex));

        }

        //consider left field
        if(currentFieldColumn > 0) {

            //left neighbor index
            int leftNeighborRowIndex = this.getIndexAsInteger(k)[0];
            int leftNeighborColumnIndex = this.getIndexAsInteger(k)[1] - 1;

            result.add(Integer.toString(leftNeighborRowIndex) + "," + Integer.toString(leftNeighborColumnIndex));
        }

        //consider top field
        if(currentFieldRow > 0) {

            //top neighbor index
            int topNeighborRowIndex = this.getIndexAsInteger(k)[0] - 1;
            int topNeighborColumnIndex = this.getIndexAsInteger(k)[1];

            result.add(Integer.toString(topNeighborRowIndex) + "," + Integer.toString(topNeighborColumnIndex));
        }

        //consider bottom field
        if(currentFieldRow < this.size-1) {

            //bottom neighbor index
            int bottomNeighborRowIndex = this.getIndexAsInteger(k)[0] + 1;
            int bottomNeighborColumnIndex = this.getIndexAsInteger(k)[1];

            result.add(Integer.toString(bottomNeighborRowIndex) + "," + Integer.toString(bottomNeighborColumnIndex));
        }
    

    return result;
    }

    public LinkedList<String> getRangeVariablesWithoutLightSource() {

        LinkedList<String> result = new LinkedList<>();

        for(int i = 0; i<this.RowRanges.size(); i++) {
            LinkedList<String> currentRange = this.RowRanges.get(i);

            int rangeSize = currentRange.size();
            int horizontalSumLight = 0;

            for(int j = 0; j<currentRange.size(); j++) {

                
                
                    String currentVariable = currentRange.get(j);

                    if(this.getVariables().get(currentVariable).equals("+")) {
                        horizontalSumLight++;
                    }
            
            }

            if(horizontalSumLight == rangeSize) {    
                for(int k = 0; k<currentRange.size(); k++) {
                    result.add(currentRange.get(k));
                }
            }
            
    }
    

    for(int i = 0; i<this.ColumnRanges.size(); i++) {
        LinkedList<String> currentRange = this.ColumnRanges.get(i);

        int rangeSize = currentRange.size();
        int verticalSumLights = 0;

        for(int j = 0; j<currentRange.size(); j++) {
            
                
                String currentVariable = currentRange.get(j);

                if(this.getVariables().get(currentVariable).equals("+")) {
                    verticalSumLights++;
                }
        }

        if(verticalSumLights == rangeSize) {
            for(int k = 0; k<currentRange.size(); k++) {
                result.add(currentRange.get(k));
            }
        }
        
    }

    return result;
    }

    public void removeValue(String variable, String value) {
        if(value.equals("*")) {
            this.domains.remove(variable, "+");
        }
        else if(value.equals("+")) {
            this.domains.remove(variable, "*");
        }
    }

    public void assignValue(String variable, String value) {
        
        
            LinkedList<String> newDomain = new LinkedList<>();
            newDomain.add(value);
            this.domains.replace(variable, newDomain);
        
        this.variables.replace(variable, value);

        this.recentlyAssignedVariables.put(variable, value);

        //check if a block is nearby, if yes then add the block to recentlyChangedBlocks
        LinkedList<String> surroundingFields = this.getSurroundingFields(variable);

        surroundingFields.forEach(
            k -> {
                if(this.blocks.containsKey(k)) {
                    this.recentlyChangedBlocks.put(k, this.blocks.get(k));
                }
            }
        );
        
    }

    public void resetValue(String variable) {
        LinkedList<String> newDomain = new LinkedList<>();
            newDomain.add("+");
            newDomain.add("*");
            this.domains.replace(variable, newDomain);
        
        this.variables.replace(variable, " ");
    }

    public boolean rangeHasLamp(LinkedList<String> range) {
        for(int i = 0; i<range.size(); i++) {
            if(this.variables.get(range.get(i)).equals("*") || this.variables.get(range.get(i)).equals(" ")) {
                return true;
            }
        }
        return false;
    }

    public void initializeRowRanges() {

        LinkedList<LinkedList<String>> result = new LinkedList<>();
        LinkedList<String> singleRange = new LinkedList<>();

        String[][] field = this.getFieldAs2DArray();

        for(int i = 0; i<this.size; i++) {

            for(int j = 0; j<this.size; j++) {
                if(field[i][j].equals(" ") || field[i][j].equals("+") || field[i][j].equals("*")) {
                    singleRange.add(Integer.toString(i) + "," + Integer.toString(j));
                }
                else if(singleRange.size() > 0){
                    result.addLast(singleRange);
                    singleRange = new LinkedList<>();
                }
                if(singleRange.size() > 0 && j ==this.size-1) {
                    result.add(singleRange);
                    singleRange = new LinkedList<>();
                }
            }
        }

        this.RowRanges = result;
    }

    public void initializeColumnRanges() {

        LinkedList<LinkedList<String>> result = new LinkedList<>();
        LinkedList<String> singleRange = new LinkedList<>();

        String[][] field = this.getFieldAs2DArray();

        for(int j = 0; j<this.size; j++) {

            for(int i = 0; i<this.size; i++) {
                if(field[i][j].equals(" ") || field[i][j].equals("+") || field[i][j].equals("*")) {
                    singleRange.add(Integer.toString(i) + "," + Integer.toString(j));
                }
                else if(singleRange.size() > 0){
                    result.add(singleRange);
                    singleRange = new LinkedList<>();
                }
                if(singleRange.size() > 0 && i ==this.size-1) {
                    result.add(singleRange);
                    singleRange = new LinkedList<>();
                }
            }
        }

        this.ColumnRanges = result;
    }

    public void initializeHintBlocks() {
        HashMap<String, String> result = new HashMap<>();

        String[][] field = this.getFieldAs2DArray();

        for(int i = 0; i<this.size; i++) {

            for(int j = 0; j<this.size; j++) {
                if(field[i][j].equals("x") || field[i][j].equals("0") || field[i][j].equals("1") || field[i][j].equals("2") || field[i][j].equals("3")|| field[i][j].equals("4")) {
                    result.put(Integer.toString(i) + "," + Integer.toString(j), field[i][j]);
                }
                
            }
        }

        this.hintBlocks = result;
    }
    


    public void displayField() {
        
        String[][] field = this.getFieldAs2DArray();

        
        System.out.println("");

        for(int k = 0; k<this.size; k++) {

            System.out.print(k);
            if(k<10) {
                System.out.print(" ");
            }
            System.out.print("[");

            for(int j = 0; j<this.size; j++) {

                if(field[k][j] == "*") {
                    System.out.print(ANSI_RED + field[k][j] + ANSI_RESET);
                }
                else if(field[k][j] == "+") {
                    System.out.print(ANSI_YELLOW + field[k][j] + ANSI_RESET);
                }
                else {
                    System.out.print(field[k][j]);
                }

                if(j<this.size-1) {
                    System.out.print(", ");
                }
            }

            System.out.println("]");
        }

        
    }

    public int[] getIndexAsInteger(String index) {
        String[] splitArr = index.split(",");

        return new int[]{Integer.parseInt(splitArr[0]), Integer.parseInt(splitArr[1])};
    }

    public String[][] getFieldAs2DArray() {
        String[][] field = new String[this.size][this.size];

        for(Map.Entry<String, String> entry : this.variables.entrySet()) {
            String k = entry.getKey();
            String v = entry.getValue();

                field[this.getIndexAsInteger(k)[0]][this.getIndexAsInteger(k)[1]] = v;
        }

        for(Map.Entry<String, String> entry : this.blocks.entrySet()) {
            String k = entry.getKey();
            String v = entry.getValue();

                field[this.getIndexAsInteger(k)[0]][this.getIndexAsInteger(k)[1]] = v;
        }
        return field;
    }

    public HashMap<String, String> getVariables() {
        return variables;
    }

    public void setVariables(HashMap<String, String> variables) {
        this.variables = variables;
    }

    

}
