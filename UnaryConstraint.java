import java.util.LinkedList;

public class UnaryConstraint implements Constraint{

        public String variable;
        public String ForbiddenValue;
        public String type = "UnaryConstraint";
    
        UnaryConstraint(String variable, String value) {
            this.variable = variable;
            this.ForbiddenValue = value;
        }
        
        @Override
        public boolean satisfied(Csp csp) {
    
                if(!csp.getVariables().get(variable).equals(ForbiddenValue) && !csp.getVariables().get(variable).equals(" ")) {
                    return true;
                }
                else {
                    return false;
                }
            
        }
    
        
        public LinkedList<String> getConstrainedVariable() {
            LinkedList<String> tmp = new LinkedList<>();
            tmp.add(variable);
            return tmp;
        }
    
    
        public String getType() {
            return this.type;
        }
        
}
