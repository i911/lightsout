import java.util.LinkedList;
import java.util.Map;

public class rangeConstraint {

    
    
    
    public boolean satisfied(Csp csp) {

         for(int i = 0; i<csp.RowRanges.size(); i++) {
                LinkedList<String> currentRange = csp.RowRanges.get(i);

                int horizontalSumLamps = 0;
                int horizontalSumLight = 0;

                for(int j = 0; j<currentRange.size(); j++) {

                    
                    
                        String currentVariable = currentRange.get(j);

                        if(csp.getVariables().get(currentVariable).equals("*")) {
                            horizontalSumLamps++;

                            if(horizontalSumLamps > 1) {
                                //System.out.println("range failed1");
                                return false;
                            }
                        }
                        /*if(csp.getVariables().get(currentVariable).equals("+")) {
                            horizontalSumLight++;

                        }*/
                
                }

                /*if(horizontalSumLight > 0 && horizontalSumLamps == 0) {
                    System.out.println("range failed1");
                    return false;
                }*/
                
        }
        

        for(int i = 0; i<csp.ColumnRanges.size(); i++) {
            LinkedList<String> currentRange = csp.ColumnRanges.get(i);

            int verticalSumLamps = 0;
            int verticalSumLights = 0;

            for(int j = 0; j<currentRange.size(); j++) {
                
                    
                    String currentVariable = currentRange.get(j);

                    if(csp.getVariables().get(currentVariable).equals("*")) {
                        verticalSumLamps++;

                        if(verticalSumLamps > 1) {
                            //System.out.println("range failed2");
                            return false;
                        }
                    }
                    /*if(csp.getVariables().get(currentVariable).equals("+")) {
                        verticalSumLights++;
                    }*/
            }

            /*if(verticalSumLights > 0 && verticalSumLamps == 0) {
                System.out.println("range failed1");
                return false;
            }*/
            
        }

        return true;

    }

    public boolean lightHasSource(Csp csp) {

        LinkedList<String> lightFields = new LinkedList<>();

        for(Map.Entry<String, String> entry: csp.variables.entrySet()) {
            if(entry.getValue().equals("+")) {
                lightFields.add(entry.getKey());
            }
        }

        

        for(int k = 0; k<lightFields.size(); k++) {

            boolean rowHasNoLampOrEmptyField = false;
            boolean columnHasNoLampOrEmptyField = false;

            String currentVariable = lightFields.get(k);

            for(int i = 0; i<csp.RowRanges.size(); i++) {
                LinkedList<String> currentRange = csp.RowRanges.get(i);

                for(int j = 0; j<currentRange.size(); j++) {

                    if(currentVariable.equals(currentRange.get(j))) {
                        if(!csp.rangeHasLamp(currentRange)) {
                            rowHasNoLampOrEmptyField = true;
                        }
                    }
            
                }

            }
    


            for(int i = 0; i<csp.ColumnRanges.size(); i++) {
                LinkedList<String> currentRange = csp.ColumnRanges.get(i);

                for(int j = 0; j<currentRange.size(); j++) {

                    if(currentVariable.equals(currentRange.get(j))) {
                        if(currentVariable.equals(currentRange.get(j))) {
                            if(!csp.rangeHasLamp(currentRange)) {
                                columnHasNoLampOrEmptyField = true;
                            }
                        }
                    }
                }
        
            }

            if(rowHasNoLampOrEmptyField && columnHasNoLampOrEmptyField) {
                return false;
            }
        }

    return true;

    }
}
