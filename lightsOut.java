public class lightsOut {
    
    public static void main(String[] args) {

    long startTime = System.nanoTime();


    backTracking game = new backTracking();
    
    Csp csp = new Csp();
    csp.initialize();
    //csp.displayField();
        
  
    csp = game.chronologicalBacktracking(csp);

    long endTime = System.nanoTime();
    long totalTime = (endTime-startTime) / 1_000_000_000;
    
    csp.displayField();

    System.out.println("Seconds of Runtime: " + totalTime);
    System.out.println("Iterations: " + game.iterations);
    System.out.println("BacktrackingSteps: " + game.backtrackingSteps);
    
    
    
    }
}
