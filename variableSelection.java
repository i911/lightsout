import java.util.Map;
import java.util.Queue;
import java.util.LinkedList;
    
    //this class has the heuristic to determine the variable
    //the chosen heuristic is the most heuristic degree
    public class variableSelection {

        Csp csp;

        variableSelection(Csp csp) {
            this.csp = csp;
        }

        //method for determining next variable
        //returns variable index as string
        public String determineNextVariable() {

            //this is the maximum heuristic degree
            //it starts with 1, so the chosen variable has at least one constraint reference
            int maxHeuristicDegree = 1;
            //collects the variables with the 
            LinkedList<String> variableWithMaxHeuristicDegree = new LinkedList<>();
            //for random decision
            LinkedList<String> unassignedVariables = new LinkedList<>();

            String currentVariable;
            int currentHeuristicDegree;

            

            for(Map.Entry<String, LinkedList<String>> entry : csp.domains.entrySet()) {

                if(entry.getValue().size() == 2) {
                unassignedVariables.add(entry.getKey());

                LinkedList<BinaryConstraint> constraints = csp.constraints;

                currentVariable = entry.getKey();
                currentHeuristicDegree = 0;

                for(int i = 0; i<csp.constraints.size(); i++) {

                    BinaryConstraint constraint = constraints.get(i);
                    String variableOne = constraint.variableOne;
                    String variableTwo = constraint.variableTwo;

                        //consider constraining references
                        if(variableOne.equals(currentVariable)) {
                            currentHeuristicDegree++;
                        }
                        if(variableTwo.equals(currentVariable)) {
                            currentHeuristicDegree++;
                        }

                }

                if(currentHeuristicDegree >= maxHeuristicDegree) {
                    maxHeuristicDegree = currentHeuristicDegree;
                    variableWithMaxHeuristicDegree.add(currentVariable);
                }
                /*else if(currentHeuristicDegree > maxHeuristicDegree){
                    variableWithMaxHeuristicDegree.clear();
                    maxHeuristicDegree = currentHeuristicDegree;
                    variableWithMaxHeuristicDegree.add(currentVariable);
                }*/
            }
            }

            System.out.println(unassignedVariables.size());
            //choose last variable in list, because it has the highest heuristic degree results in longer search time
            //choose first variable in list, results in shorter search time
            if(variableWithMaxHeuristicDegree.size() > 0) {
                int result = (int)(Math.random() * variableWithMaxHeuristicDegree.size());
                
                return variableWithMaxHeuristicDegree.getFirst();
            }
            else if(unassignedVariables.isEmpty()) {
                return null;
            }
            else if(variableWithMaxHeuristicDegree.size() == 0) {
                int result = (int)(Math.random() * unassignedVariables.size());
                
                return unassignedVariables.get(result);
            }
            else {
                
                return variableWithMaxHeuristicDegree.getFirst();
            }


            



        }
    }