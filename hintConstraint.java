import java.util.LinkedList;
import java.util.Map;

public class hintConstraint {

    
    
    public boolean satisfied(Csp csp) {

        

        for(Map.Entry<String, String> entry : csp.hintBlocks.entrySet()) {

            int lampCount = 0;
            int lightCount = 0;

            if(!entry.getValue().equals("x")) {

            String currentBlock = entry.getKey();
            int blockValue = Integer.parseInt(entry.getValue());

            LinkedList<String> surroundingFields = csp.checkSurroundingFields(currentBlock);

            for(int i = 0; i<surroundingFields.size(); i++) {
                
                    if(surroundingFields.get(i).equals("*")) {
                        lampCount++;

                        if(lampCount > blockValue) {
                            //System.out.println("hint failed1");
                            
                            return false;
                        }
                    }

                    if(surroundingFields.get(i).equals("+") || surroundingFields.get(i).equals("x") || surroundingFields.get(i).equals("0") || surroundingFields.get(i).equals("1") || surroundingFields.get(i).equals("2") || surroundingFields.get(i).equals("3") || surroundingFields.get(i).equals("4")) {
                        lightCount++;

                        if(lightCount > surroundingFields.size()-blockValue) {
                            //System.out.println("hint failed2");
                            return false;
                        }
                    }
                }
            
        }

    
    }
    return true;
}
}
