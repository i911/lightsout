import java.io.*;
import java.util.Arrays;

public class csvReader10x10 {    
    

    public static void main(String[] args) {
        getField();
    }

   static public String[][] getField() {
        
            //current line that is read
            String line = "";  
            String splitBy = "";  
            //resulting lights out field
            String[][] result = new String[10][10];
            //counter for iterating through result field
            int counter = 0;

            
            try   {  
            //parsing a CSV file into BufferedReader class constructor  
            BufferedReader br = new BufferedReader(new FileReader("lights10x10.csv"));  
            while ((line = br.readLine()) != null)   //iterating through csv rows 
            {  
                String[] field = line.split(splitBy);    // use empty as separator
                //put empty spaces between two semicolons
                String repl = line.replaceAll("(?<=;)(?=;)", " ");
               
                //turn String into array
                String[] arr = repl.split("");

                //put empty space in front when there is no value
                if(arr[0].equals(";")) {
                    arr[0] = " ";
                }
                //put empty space in back when there is no value
                if(arr[arr.length-1].equals(";")) {
                    arr[arr.length-1] = " ";
                }
                
                //variable for iterating through csv array
                int k = 0;
                for(int i = 0; i<10; i++) {
                    
                    while(k < arr.length) {

                        //when empty space in array, put empty space in result
                        if(arr[k].equals(" ")) {
                            result[counter][i] = arr[k];
                            k++;
                            break;
                        }
                        //when semicolon in array, skip it
                        else if(arr[k].equals(";")) {
                            k++;
                        }
                        //when actual value in array, copy it to result
                        else if(!arr[k].equals(";") && !arr[k].equals(" ")) {
                            result[counter][i] = arr[k];
                            k++;
                            break;
                        }
                    }
                }

                
                //next row
                counter++;
            }  
            }   
            catch (IOException e)   
                {  
                e.printStackTrace();  
                }  
                
            
            return result;
    }
}
