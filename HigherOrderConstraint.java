import java.util.LinkedList;

public class HigherOrderConstraint implements Constraint{
    
    LinkedList<String> variables = new LinkedList<>();
    //can also be a block
    String constrainingVariable;

    int value;

    public String type = "HigherOrderConstraint";

    

    public HigherOrderConstraint(String variableOne, String variableTwo, String variableThree, String variableFour, int value, String constrainingVariable) {
        if(variableOne!=null) {
            variables.add(variableOne);
        }
        if(variableTwo!=null) {
            variables.add(variableTwo);
        }
        if(variableThree!=null) {
            variables.add(variableThree);
        }
        if(variableFour!=null) {
            variables.add(variableFour);
        } 
        this.value = value;
        this.constrainingVariable = constrainingVariable;
    }


    //check if two are equal and one is unequal
    @Override
    public boolean satisfied(Csp csp) {

        int counterOfLamps = 0;
        
        switch(this.value) {
            case 1:
            for(int i = 0; i<variables.size(); i++) {
                if(csp.variables.get(variables.get(i)).equals("*")) {
                    counterOfLamps++;
                }
            }
            break;
            case 2:
            for(int i = 0; i<variables.size(); i++) {
                if(csp.variables.get(variables.get(i)).equals("*")) {
                    counterOfLamps++;
                }
            }
            break;
            case 3:
            for(int i = 0; i<variables.size(); i++) {
                if(csp.variables.get(variables.get(i)).equals("*")) {
                    counterOfLamps++;
                }
            }
            break;
            case 4:
            for(int i = 0; i<variables.size(); i++) {
                if(csp.variables.get(variables.get(i)).equals("*")) {
                    counterOfLamps++;
                }
            }
            break;
        }

        if(counterOfLamps == this.value) {
            return true;
        }
        else {
            return false;
        }
    }


    public LinkedList<String> getConstrainedVariable() {
        return variables;
    }

    public String getType() {
        return this.type;
    }
}
